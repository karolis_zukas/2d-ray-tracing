class Particle {
    constructor() {
        this.position = createVector(width / 2, height / 2);
        this.rays = [];
        for (let angle = 0; angle < 360; angle += 1) { //every n degrees of circle
            this.rays.push(new Ray(this.position, radians(angle)));
        }
    }

    look(walls) {
        for (let ray of this.rays) {
            let closestPt = null;       // Closest point of intersection
            let record = Infinity;
            for(let wall of walls) {
                const pt = ray.cast(wall);
                if (pt) {
                    const d = p5.Vector.dist(this.position, pt);
                    if (d < record) {
                        record = d;
                        closestPt = pt;
                    }
                }
            }
            if (closestPt) {
                stroke(255, 100);
                line(this.position.x, this.position.y, closestPt.x, closestPt.y);
            }
        }
    }

    update(x, y) {
        this.position.set(x, y);
    }

    show() {
        fill(255);
        ellipse(this.position.x, this.position.y, 4);

        for (let ray of this.rays) {
            ray.show();
        }
    }
}